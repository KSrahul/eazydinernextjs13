/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  images: {
    domains: ["d4t7t8y8xqo0t.cloudfront.net"]
  },
}

module.exports = nextConfig
