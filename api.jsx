const env = process.env.NODE_ENV;
const YODA_API_URL = process.env.NEXT_PUBLIC_YODA_API_ENDPOINT;
const NS_API_URL = process.env.NEXT_PUBLIC_NS_API_ENDPOINT;
const PROD_API_URL = process.env.NEXT_PUBLIC_PRODUCTION_API_ENDPOINT;
let ED_API_URL = "";
if(env == "development"){
    ED_API_URL = NS_API_URL;
}else{
    ED_API_URL = NS_API_URL;
} 


export const RESTAURANT_DETAILS = `${ED_API_URL}/5.1/restaurants`;
export const RESTAURANT_LISTING = `${ED_API_URL}/4.4`;
export const homePageData = (city) =>{return `${ED_API_URL}/5.2/explore/${city}`};
export const locationSearch = (searchText) => {return `${ED_API_URL}/4.1/autocomplete?keyword=${searchText}&type=location`};
export const checkUser = `${ED_API_URL}/4.0/check-user`;
export const otpSend = `${ED_API_URL}/4.0/otp`;
export const register = `${ED_API_URL}/4.1/login`;
export const userDataApi = (customerId) => {return `${ED_API_URL}/4.1/customers/${customerId}`};
export const dummyData = `${ED_API_URL}/4.0/payeazy/offer-list?medium=android&restaurant_id=delhi-ncr%2Fblue-tokai-galleria-market-gurgaon-666013`;
export const myBookings = (customerId) => {return `${ED_API_URL}/4.2/customers/${customerId}/booking-listing`};
export const checkReferralCode = (checkReferral) => {return `${ED_API_URL}/4.0/check-referral-code?referral_code=${checkReferral}`}
export const FOOD_TRENDS = `${ED_API_URL}/4.0/food-trends`;
export const FOOD_TRENDS_DETAILS = `${ED_API_URL}/4.1/food-trends`;