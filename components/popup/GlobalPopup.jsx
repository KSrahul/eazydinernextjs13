import LoginSignup from '@/components/loginSignup/LoginSignup';
import HalfPopup from '@/components/popup/HalfPopup';
import LocationMain from '@/components/location/LocationMain';
import RestaurantSearch from '@/components/restaurantSearch/RestaurantSearch'
import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { MODAL_LOCATION, MODAL_SEARCH_RESTAURANTS, MODAL_LOGIN } from '@/context/ActionType';


export default function GlobalComponents(props) {
  const {states} = ContextAllUpdate();
  return (
    <>
      {
        states.defaultModal != "" &&
        <>
          <HalfPopup title={states.userAuthTitle} 
            id={MODAL_LOGIN}>
              <LoginSignup />
          </HalfPopup>

          <HalfPopup title={`Search Location`} 
            id={MODAL_LOCATION}>
              <LocationMain />
          </HalfPopup>

          <HalfPopup title={`Search Restaurants`} 
            id={MODAL_SEARCH_RESTAURANTS}>
              <RestaurantSearch />
          </HalfPopup>
        </>
      }
    </>
  )
}