import { REMOVE_MODAL } from "@/context/ActionType";
import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { useState, useEffect } from "react";

export default function PopupMain(props) {
    const [maxHeight, setMaxHeight] = useState("")
    const {title, id} = props
    const {states, updateStates} = ContextAllUpdate();

    const closePopup = () => {
        updateStates({
            type: REMOVE_MODAL,
        })
    }

    useEffect(() => {
        setMaxHeight(`${window.innerHeight - 100}px`)
    }, [])
  return (
        <>        
            {
                id && id == states.defaultModal &&
                <>
                    <div className="modal_main bg-white fixed full-width">
                        <div className="modal_header paddding-15 flex align-v-center flex-between" onClick={closePopup}>
                            <div className='modal_title font-18 grey-dark semi-bold'>{title}</div>
                            <div>
                                <svg width="15" height="10" viewBox="0 0 15 10" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m1.5.5 6 5.953L13.5.5 15 2 7.5 9.5 0 2z" fill="#212121" fillRule="evenodd"></path>
                                </svg>
                            </div>
                        </div>
                        <div className='modal_content_main bg-white'>
                            <div className="modal_content paddding-15" style={{"maxHeight" : maxHeight}}>
                                {props.children}
                            </div>
                        </div>
                    </div>
                    <div className="modal_backdrop bg-black top-0 full-width full-height fixed" onClick={closePopup}></div>
                </>
            }
        </>
  )
}
