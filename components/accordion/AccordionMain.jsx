import { useState } from "react"
import downAero from "public/icons/login/down-arrow.svg"

export const AccordionMain = (props) => {
    const {activePanel, id, title, content} = props;
    const [isActive, setIsActive] = useState(activePanel)
    return (
        <div id={id} className="accordion-item">
            <div className="accordion-title" onClick={() => setIsActive(!isActive)}>
            <div className="flex flex-between pointer padding-5">
                <div className="font-16 semi-bold grey-dark">{title}</div>
                <div className={`${isActive ? 'rotate_accordion' : ''}`}>
                    <img src={downAero.src} alt=""  />
                </div>
            </div>
        </div>

        {isActive && (
          <AccordionContent>
              {content}
          </AccordionContent>
        )}
      </div>
    )
}

export const AccordionContent = ({children}) => {
    return (
        <div className="accordion-content">
            <div>{children}</div>
        </div>
      )
}
