import MainHeader from '@/components/headers/MainHeader';
import {keyConstant} from '../constant';
import BottomFloatingNav from './footer/BottomFloatingNav';
import dynamic from 'next/dynamic';
import {RouterObject} from '@/helpers/RouterObject';
import { useEffect } from 'react';
const GlobalPopup = dynamic(() => import("@/components/popup/GlobalPopup"), {ssr: false,})
import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { REMOVE_MODAL } from "@/context/ActionType";
import {SeoMeta, SeoDefaultMeta} from "@/components/seo/SeoMeta"


export default function Layout(props) {
  const {states, updateStates} = ContextAllUpdate();
  const router = RouterObject();
  const {pageName, userDetails, userLocation, metaData} = props.children.props;
  const checkPage = [keyConstant.HOME, keyConstant.BOOKING, keyConstant.PROFILE, keyConstant.PRIME, keyConstant.PAYEAZY];


  useEffect(() => {
    router.events.on("routeChangeStart", ()=>{
      updateStates({
        type: REMOVE_MODAL,
      })
    })
  }, [])
  return (
    <>
      <GlobalPopup />
        <SeoMeta metaData={metaData}/>
        {
          pageName == keyConstant.HOME &&
          <MainHeader userDetails={userDetails} userLocation={userLocation} />
        }

        {props.children}
        
        {
          checkPage.includes(pageName) && <BottomFloatingNav pageName={pageName} userDetails={userDetails} />
        }
    </>
  )
}
