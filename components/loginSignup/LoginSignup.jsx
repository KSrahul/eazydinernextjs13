import { useState, useEffect, useRef} from 'react';
import styles from 'styles/login.module.css';
import {ClientSide} from '@/helpers/FetchData';
import { checkReferralCode, checkUser, otpSend, register } from 'api';
import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { AUTH_TITLE } from '@/context/ActionType';
import FloatInput from "@/components/floatInput/FloatInput"
import { InactiveBtn, ActiveBtn, LoaderBtn } from '@/components/buttons/Buttons';
import {RouterObject} from '@/helpers/RouterObject';
import { setCookie } from 'cookies-next';
import PhoneNumber from "@/components/phoneNumber/PhoneNumber"


export default function LoginSignup(props) {
  const otpInput = useRef([]);
  const router = RouterObject();
  const otpFields = {otp0: "", otp1: "", otp2: "", otp3: ""}
  const formFields = { mobile: "", countryCode: "", otp: otpFields,name: "",email: "",dob: "",anniversary: "",referralCode: "" };
  const {updateStates} = ContextAllUpdate();
  const [userDetails, setUserDetails] = useState(formFields)
  const [booleanValues, setBooleanValues] = useState({
      showSignupAction: false,
      btnLoader: false,
      showLogin: true,
      showOtp: false,
      showSignup: false,
      showReferral: false
  })

  const updateAuthTitle = (title) => {
    updateStates({
      type: AUTH_TITLE,
      data: title
    })
  }

  const inputValue = (e) => {
    const {name, value} = e.target
    setUserDetails({ ...userDetails, [name]: value });
    if(name == "mobile"){
      otpInput.current[0] = e.target
    }
  }


  const [ userLoading, userValidateData, userError, validateUserNumber, resetCheckUser ] = ClientSide(
    {
      url: `${checkUser}`,
      method: 'post',
      data: {
        "mobile": `+${userDetails.countryCode}${userDetails.mobile}`,
      }
    },
  );

  const [ otpLoading, sendOtpData, otpError, sendUserOtp, resetOtpSend ] = ClientSide(
    {
      url: `${otpSend}`,
      method: 'get',
      params: {
        "mobile": `+${userDetails.countryCode}${userDetails.mobile}`,
      },
    },
    userValidateData.data && !userValidateData.data.data.is_new_user
  );

  const [ registerLoading, registerData, registerError, registerUser ] = ClientSide(
    {
      url: `${register}`,
      method: 'post',
      data: {
        "mobile": `+${userDetails.countryCode}${userDetails.mobile}`,
        "otp": `${Object.values(userDetails.otp).join('')}`,
        "medium": `android`,
        "device_id": `152.58.121.139`,
        "dob": `${userDetails.dob}`,
        "name": `${userDetails.name}`,
        "whatsappoptin": `1`,
        "email": `${userDetails.email}`,
        "anniversary": `${userDetails.anniversary}`,
      }
    },
    Object.values(userDetails.otp).join('').length >= 4 && Object.values(userDetails.otp).join('')
  );

  const [ referralLoading, referraData, referralError, sendReferral, resetReferral ] = ClientSide(
    {
      url: `${checkReferralCode(userDetails.referralCode)}`,
      method: 'get',
    },
    false
  );
  
  const validateUser = (e) => {
    validateUserNumber()
    setBooleanValues({ ...booleanValues, "btnLoader": true });
  }

  const editNumber = () => {
    resetOtpSend();
    resetCheckUser();
    setBooleanValues({ ...booleanValues, 
      "showLogin": true, 
      "showOtp": false,
      "showSignup": false,
      "btnLoader": false
    });
    updateAuthTitle("Enter your mobile number")
  }

  const submitOtp = (e) => {
    if(!userDetails.referralCode){
      sendUserOtp()
      setBooleanValues({ 
      ...booleanValues, 
        "showSignup": false,
        "showLogin": false,
        "showOtp": true,
      });
    }else{
      sendReferral();
    }
  }

  const signupUser = () => {    
    registerUser()
  }

  useEffect(() => {
    if(sendOtpData){
      setBooleanValues({
         ...booleanValues, 
         "showLogin": false,
         "showOtp": true
      });
      updateAuthTitle("Confirm your number")
    }

    if(!sendOtpData && userValidateData.data && userValidateData.data.data.is_new_user){
      let checkValue = false
      const emailValidate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(userDetails.name && userDetails.email && emailValidate.test(userDetails.email)){
        checkValue = true
      }else{
        checkValue = false
      }
      setBooleanValues({ 
        ...booleanValues, 
          "showSignup": true,
          "showLogin": false,
          "showOtp": false,
          "showSignupAction": checkValue
      });
      updateAuthTitle("We need your basic Info")
    }

    if(registerData){
      setCookie("token", registerData.data.token);
      setCookie("cusId", registerData.data.data.id);
      location.reload();
      console.log("Yes success", router)
    }

    if(userError && userError.response.data || otpError && otpError.response.data){
      setBooleanValues({ ...booleanValues, "btnLoader": false });
      resetCheckUser();
      resetOtpSend();
      alert("Error!")
    }

  }, [sendOtpData, userValidateData, registerData, userError, otpError, userDetails])

  useEffect(() => {
    if(referraData && !referraData.data.data.status){
      alert("Wrong referral code :(")
    }
  }, [referraData])

  const otpKeyUp = (e) => {
    const {name, value} = e.target
    if(e.target.value && e.target.nextElementSibling){
      e.target.nextElementSibling.focus();
    }
    setUserDetails({
       ...userDetails,
       "otp" : {...userDetails['otp'], [name] : value}
    });
  }

  const checkBackSpace = (e) => {
    if(e.key == "Backspace" &&  e.target.value == "" && e.target.previousElementSibling){
      e.target.previousElementSibling.focus();
    }
  }

  const showCalender = () => {
    console.log("clicked")
  }
  const showReferralCode = () => {
    setBooleanValues({ ...booleanValues, "showReferral": true });
  }

  return (
      <> 
        {
          booleanValues.showLogin &&
            <div className={`${styles.space_login}`}>
                <div className={`flex align-v-center with_country relative`}>
                    <div className='country_code_main'>
                      <PhoneNumber
                        onMount ={(value, data) => {
                            setUserDetails({ ...userDetails, "countryCode": value});
                        }}
                        onChange ={(value, data) => {
                            setUserDetails({ ...userDetails, "countryCode": value, "mobile" : "" });
                            otpInput.current[0]?.focus()
                        }}
                      />
                        <div className="flex align-v-center country_aero relative">
                            <div className="country_code grey font-15">+{userDetails.countryCode}</div>
                            <div className="flex margin-l-5">
                                <svg width="9" height="6" viewBox="0 0 9 6" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m4.571 4.428 3.334-3.403a.62.62 0 0 1 .89 0 .652.652 0 0 1 0 .91L5.017 5.79a.62.62 0 0 1-.89 0L.346 1.934a.652.652 0 0 1 0-.909.62.62 0 0 1 .891 0l3.333 3.403z" fill="#616161" fillRule="nonzero"></path>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div className="full-width">
                      <FloatInput 
                        type="tel" 
                        name={"mobile"} 
                        label="Mobile Number" 
                        handleChange={inputValue} 
                        value={userDetails.mobile} 
                        required={true}
                        autoFocus={true}
                      />
                    </div>
                </div>
                <div className={`grey font-12 padding-t-5`}>We’ll text you to confirm your number.</div>

                <div className={`${styles.send_otp_main}`}>
                  {
                    userDetails.mobile.length >= 10 && !booleanValues.btnLoader ?
                      <div onClick={validateUser}>
                        <ActiveBtn title="Get OTP" />
                      </div>
                    : userDetails.mobile.length < 10 && !booleanValues.btnLoader ?
                        <InactiveBtn title="Get OTP" />
                    : <LoaderBtn title="Loading..." />
                  }
                </div>
            </div>
        }

        {
          booleanValues.showOtp &&
            <div className={`${styles.space_login}`}>
                <div className='flex align-v-center padding-b-10'>
                    <div className={`grey font-12`}>Enter the 4 digit OTP we’ve sent by SMS to {userDetails.mobile} </div>
                    <div className="text-blue pointer margin-l-5" onClick={editNumber}>
                        Edit
                        <svg className="margin-l-2" width="11" height="11" viewBox="0 0 11 11" xmlns="http://www.w3.org/2000/svg">
                            <defs>
                                <path id="h9ac8ijxsa" d="M0 0h10.627v10.63H0z"></path>
                            </defs>
                            <g fill="none" fillRule="evenodd">
                                <mask id="78off709ib" fill="#fff">
                                    <use xlinkHref="#h9ac8ijxsa"></use>
                                </mask>
                                <path d="m9.204 3.012.33-.33a.827.827 0 0 0 0-1.167l-.42-.42a.825.825 0 0 0-1.166 0l-.33.33 1.586 1.587zM3.476 8.739 8.6 3.615 7.013 2.03 1.891 7.153 3.476 8.74zm-2.436.85 1.628-.45-1.177-1.177-.45 1.628zm-.612 1.04a.426.426 0 0 1-.413-.54l.84-3.03a.425.425 0 0 1 .11-.187L7.344.492C7.66.174 8.084 0 8.53 0c.45 0 .87.174 1.187.492l.42.42a1.682 1.682 0 0 1 0 2.374l-6.38 6.38a.428.428 0 0 1-.188.109l-3.03.84a.414.414 0 0 1-.112.014z" fill="#4A90E2" fillRule="nonzero" mask="url(#78off709ib)"></path>
                            </g>
                        </svg>
                    </div>
                </div>
                <div className={`flex align-v-center`}>
                    {
                      Array(4).fill().map((item, index) => {
                        return(
                          <input key={index} 
                            className="full-width float_input otp_inputs radius-8 h-50 margin-r-20 outline-0 text-center" 
                            name={`otp${index}`}
                            type="tel" 
                            maxLength={1}
                            onChange={otpKeyUp}
                            onKeyDown={checkBackSpace}
                            value={userDetails.otp[`otp${index}`]}
                            autoFocus={index === 0}/>
                        )
                      })
                    }
                </div>
                  
                  <div className={`${styles.send_otp_main}`}>
                    {
                      Object.values(userDetails.otp).join('').length >= 4 && registerLoading ?
                        <LoaderBtn title="Loading" />
                      : <InactiveBtn title="Enter OTP" />
                    }
                  </div>
            </div>
        }

        {
          booleanValues.showSignup &&
            <div className={`${styles.space_login}`}>
                <div className="full-width margin-b-15">
                  <FloatInput 
                    type="text" 
                    name={"name"}
                    handleChange={inputValue}
                    label="Name" 
                    value={userDetails.name} 
                    required={true} 
                    autoFocus={true}
                  />
                </div>
                <div className="full-width margin-b-15">
                  <FloatInput 
                    type="tel" 
                    name={"email"}
                    handleChange={inputValue} 
                    label="Email" 
                    value={userDetails.email} 
                    required={true} 
                  />
                </div>

                <div className="flex margin-b-15">
                  <div className="full-width margin-r-10 relative">
                    <FloatInput 
                      type="tel" 
                      name={"dob"}
                      handleChange={inputValue} 
                      label="Date of birth" 
                      value={userDetails.dob} 
                      required={true} 
                    />
                    <input onChange={inputValue} value={userDetails.dob} className="default_date_picker top-0 full-width full-height left-0 absolute" id="user_birth" name="dob" type="date"></input>
                  </div>
                  
                  <div className='full-width relative'>
                    <FloatInput 
                      type="tel" 
                      name={"anniversary"}
                      handleChange={inputValue} 
                      label="Anniversary" 
                      value={userDetails.anniversary} 
                      required={true} 
                    />
                    <input onChange={inputValue} value={userDetails.anniversary} className="default_date_picker top-0 full-width full-height left-0 absolute" id="user_anniversary" name="anniversary" type="date"></input>
                  </div>
                </div>
                <div className='grey-light-dark margin-b-15'>Other people who use EazyDiner won't see your date of birth and anniversary date.</div>

                {
                  !booleanValues.showReferral && 
                  <div className='text-blue bold' onClick={showReferralCode}>Have a referral code?</div>
                }

                {
                  booleanValues.showReferral && 
                  <div onClick={showCalender}>
                    <FloatInput 
                      type="tel" 
                      name={"referralCode"}
                      handleChange={inputValue} 
                      label="Referral code" 
                      value={userDetails.referralCode} 
                      required={true} 
                    />
                  </div>
                }

                <div className='grey-light-dark margin-t-15 margin-b-15'>
                  By Clicking Sign Up, you are agree to our <a className='' href='/contact'>Terms of Service</a> & that you have read our <a className='' href='/contact'>Privacy Policy</a>
                </div>

                <div className={`${styles.send_otp_main}`}>
                  {
                    booleanValues.showSignupAction ?
                      <div onClick={submitOtp}>
                        <ActiveBtn title="Signup now" />
                      </div>
                    : <InactiveBtn title="Signup now" />
                  }
                </div>
            </div>
        }
      </>
  )
}