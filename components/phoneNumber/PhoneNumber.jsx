import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/material.css'

export default function PhoneNumber(props) {
    const {onMount, onChange} = props
  return (
    <>
        <PhoneInput
            containerClass='custom_class'
            country={'in'}
            preferredCountries={['in','ae']}
            enableSearch={true}
            disableSearchIcon={false}
            countryCodeEditable={false}
            onMount={onMount}
            onChange={onChange}
        />   
    </>
  )
}
