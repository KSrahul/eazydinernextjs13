import HeaderBackNav from '@/components/headers/HeaderBackNav'
import { ClientSide } from '@/helpers/FetchData'
import { myBookings } from 'api'



export default function MyBookings(props) {
    const {userData, tokenId} = props
    const [ loadingBookings, bookingData, bookingsError ] = ClientSide(
      {
        url: `${myBookings(tokenId.cusTomerId)}`,
        method: 'get',
        headers:{
          "Authorization": `Bearer:${tokenId.token}`,
        }
      },
      true
    );

    console.log(loadingBookings, bookingData, bookingsError)
    
  return (
    <>
        <HeaderBackNav title="My Bookings" hideBack={true} />
        <div className='back_header_content padding-15'>
            <h2 className=''> Hi, {userData.name}! <br /><br /> Your booking is comming soon...</h2>
        </div>
    </>
  )
}
