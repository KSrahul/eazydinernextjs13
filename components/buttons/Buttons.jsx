export const InactiveBtn = (props) => {
    const {title} = props;
    return(
        <div className="white pointer relative text-center semi-bold full-width btns_main inactive_main">
            {title}
            <div className="absolute btns_aero">
                <svg width="18" height="16" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.932 7.655a.913.913 0 0 1 0 .689.912.912 0 0 1-.194.29l-7.099 7.104a.893.893 0 1 1-1.263-1.264l5.575-5.58H.892a.894.894 0 0 1 0-1.789h14.06L9.377 1.528A.897.897 0 0 1 10.007 0c.23 0 .459.087.633.261l7.1 7.106c.017.017.022.04.037.057.06.07.118.143.155.231z" fill="white" fillRule="nonzero"></path>
                </svg>
            </div>
        </div>
    )
}

export const ActiveBtn = (props) => {
    const {title} = props;
    return(
        <div className="white pointer relative text-center font-15 semi-bold full-width btns_main active_main">
            {title}
            <div className="absolute btns_aero">
                <svg width="18" height="16" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.932 7.655a.913.913 0 0 1 0 .689.912.912 0 0 1-.194.29l-7.099 7.104a.893.893 0 1 1-1.263-1.264l5.575-5.58H.892a.894.894 0 0 1 0-1.789h14.06L9.377 1.528A.897.897 0 0 1 10.007 0c.23 0 .459.087.633.261l7.1 7.106c.017.017.022.04.037.057.06.07.118.143.155.231z" fill="white" fillRule="nonzero"></path>
                </svg>
            </div>
        </div>
    )
}

export const LoaderBtn = (props) => {
    const {title} = props;
    return(
        <div className="white pointer relative text-center font-15 semi-bold full-width btns_main active_main">
            {title}
            <div className="absolute btns_aero btns_aero_loder">
                <div className="spinner1"></div>
            </div>
        </div>
    )
}

export const ShowMoreBtn = (props) => {
    const {title} = props;
    return(
        <div className="black pointer relative text-center font-15 semi-bold full-width btns_main show_more_loader">
            {title}
        </div>
    )
}


export const ShowMoreLoader = (props) => {
    const {title} = props;
    return(
        <div className="black pointer relative text-center font-15 semi-bold full-width btns_main show_more_loader">
            {title}
            <div className="absolute btns_aero btns_aero_loder">
                <div className="spinner1"></div>
            </div>
        </div>
    )
}