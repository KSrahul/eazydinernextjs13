import SearchInput from '@/components/searchInput/SearchInput'
import ContextAllUpdate from "@/helpers/ContextAllUpdate"

export default function RestaurantSearch() {
    const {states} = ContextAllUpdate()
  return (
    <>
        <SearchInput placeholder="Search restaurants, locations or cuisines" />
        <br />
        {
            states.searchResult && <div className='text-center' style={{"fontSize": "20px"}}>{states.searchResult}</div>
        }
    </>
  )
}