import LocationBox from '@/components/location/LocationBox'
import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { MODAL_LOGIN, MODAL_SEARCH_RESTAURANTS, SHOW_MODAL } from '@/context/ActionType';
import LogoutUserFn from '@/helpers/LogoutUser';



export default function MainHeader(props) {
  const {userDetails, userLocation} = props
  const userData = userDetails.data
  const {updateStates} = ContextAllUpdate();

  const showModal = (id) => {
    updateStates({
      type: SHOW_MODAL,
      modalToShow: id
    })
  }
  
  const logoutUser = () => {
    const {logout} = LogoutUserFn();
  }
  return (
    <>
      <div className={`header_color top-0 full-width fixed bg-white black ${userData && userData.is_prime ? 'prime_user' : 'non_prime_home'}`}>

        <div className='space_header_home'>
          <div className='flex flex-between'>

            <div className='flex align-v-center'>
              <div className="flex pointer">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width={24} height={24}>
                    <g fill="none" fillRule="evenodd">
                        <path fill="#FA4616" fillRule="nonzero" d="M19.535 9.768c0 6.22-7.021 11.98-7.021 11.98-.41.336-1.082.336-1.492 0 0 0-7.022-5.76-7.022-11.98a7.768 7.768 0 0115.535 0zm-7.767 3.907a3.912 3.912 0 003.907-3.908 3.912 3.912 0 00-3.907-3.907A3.912 3.912 0 007.86 9.768a3.912 3.912 0 003.908 3.907z"></path>
                        <path d="M0 0H24V24H0z"></path>
                    </g>
                </svg>
              </div>
              <LocationBox userLocation={userLocation} />
            </div>
            {
              userData ? 
                <div className='flex login_btn_home font-14 white align-v-center' onClick={logoutUser}>Logout</div>
              : <div className='flex login_btn_home font-14 white align-v-center' onClick={() => showModal (MODAL_LOGIN)}>Login</div>
            }

          </div>

          <div className='relative flex search_main_home radius-5 margin-t-10' onClick={() => showModal (MODAL_SEARCH_RESTAURANTS)}>
            <div className="search_icon absolute flex">
              <svg width="18" height="18" viewBox="0 0 22 22">
                <g fill="none" fillRule="evenodd">
                    <path fill="#9E9E9E" fillRule="nonzero" d="M18.465 17.319l-3.728-3.81a6.13 6.13 0 001.484-3.995c0-3.426-2.838-6.214-6.325-6.214-3.488 0-6.325 2.788-6.325 6.214 0 3.426 2.837 6.214 6.325 6.214 1.309 0 2.557-.388 3.623-1.124l3.757 3.838c.157.16.368.249.595.249.214 0 .417-.08.571-.226a.801.801 0 00.023-1.146zM9.895 4.92c2.578 0 4.676 2.06 4.676 4.593s-2.098 4.593-4.675 4.593c-2.578 0-4.675-2.06-4.675-4.593s2.097-4.593 4.675-4.593z"></path>
                    <path d="M0 0H22V22H0z"></path>
                </g>
              </svg>
            </div>
            <div className='search_input_home font-14 margin-l-20'>Search restaurants, locations or cuisines</div>
          </div>
          
        </div>
      </div>
    </>
    
  )
  
}