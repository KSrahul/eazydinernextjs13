
import HeaderBackNav from '@/components/headers/HeaderBackNav'

export default function MyPrime(props) {
    const {userData} = props
  return (
    <>
        <HeaderBackNav title="My Prime" hideBack={true}/>
        <div className='back_header_content padding-15'>
            <h2 className=''> Hi, {userData.name}! <br /><br /> Your prime is comming soon...</h2>
        </div>
    </>
  )
}
