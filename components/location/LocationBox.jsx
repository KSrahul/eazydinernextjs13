import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { MODAL_LOCATION, SHOW_MODAL } from '@/context/ActionType';

export default function LocationBox(props) {
    const {userLocation} = props
    const {updateStates} = ContextAllUpdate();

    const searchLocation = () => {
        updateStates({
            type: SHOW_MODAL,
            modalToShow: MODAL_LOCATION,
        })
    }
  return (
    <div>
        <div className='flex full-width v-flex-end' onClick={searchLocation}>
            <div className='loc_main_home'>
                <div className='loc_text_home font-12 padding-b-2'>{userLocation.city_name}</div>
                <div className='selected_loc_home font-15 semi-bold'>{userLocation.code}</div>
            </div>
            <div className='loc_aero'>
                <svg width="12" height="12">
                    <g fill="none" fillRule="evenodd">
                        <path className='loc_aero_down' fill="black" fillRule="nonzero" d="M5.975 6.61l4.33-4.242a.79.79 0 000-1.133.83.83 0 00-1.156 0l-4.91 4.809a.79.79 0 000 1.133l4.91 4.809a.83.83 0 001.157 0 .79.79 0 000-1.133L5.976 6.61z" transform="rotate(-90 7 7)"></path>
                        <path d="M0 0H14V14H0z" transform="rotate(-90 7 7)"></path>
                    </g>
                </svg>
            </div>
        </div>
    </div>
  )
}