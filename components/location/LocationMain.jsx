import SearchInput from '@/components/searchInput/SearchInput'
import defaultLocation from "@/public/icons/location/default_location.svg"
import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { ClientSide } from '@/helpers/FetchData'
import { locationSearch } from 'api'
import { setCookie } from 'cookies-next';

export default function LocationMain() {
    const {states} = ContextAllUpdate();
    const textLength = states.searchResult.length > 1;
    const [ locationLoading, locationData, locationError ] = ClientSide(
      {
        url: `${locationSearch(states.searchResult)}`,
        method: 'get',
      },
      textLength
    );

    const setLocation = (e) => {
        setCookie("userLocation", e);
        location.reload()
    }
  return (
    <div className='location_internal_main'>
        <div>
            <div className='loc_box margin-b-15 radius-4 padding-15 flex text-center align-v-center pointer flex-center'>
                <div className='flex'>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 18 18">
                        <g fill="#FFF" fillRule="evenodd">
                            <path d="M8.995 5.93a3.067 3.067 0 00-3.063 3.065 3.067 3.067 0 003.063 3.063 3.067 3.067 0 003.064-3.063A3.067 3.067 0 008.995 5.93z"></path>
                            <path fillRule="nonzero" d="M17.473 8.467h-1.936a6.574 6.574 0 00-6.015-6.015V.527a.527.527 0 00-1.054 0v1.925a6.574 6.574 0 00-6.015 6.015H.527a.527.527 0 000 1.055h1.926a6.574 6.574 0 006.015 6.015v1.925a.527.527 0 001.054 0v-1.925a6.574 6.574 0 006.015-6.015h1.936a.527.527 0 000-1.055zm-8.478 6.036a5.515 5.515 0 01-5.509-5.508 5.515 5.515 0 015.51-5.51 5.515 5.515 0 015.508 5.51 5.515 5.515 0 01-5.509 5.508z"></path>
                        </g>
                    </svg>
                </div>
                <div className='white current_loc_text margin-l-10 font-15 semi-bold'>Use my Current Location</div>
            </div>
            <SearchInput placeholder="Type a city to find your location"/>
        </div>

        <div className='margin-t-20'>
            {
            textLength && locationData.data && locationData.data.data &&
                locationData.data.data.map((e, i) => {
                    return(                   
                        <div onClick={() => setLocation(e)} key={i} className='flex align-v-center loc_result margin-b-20 pointer'>
                            <div className='flex'>
                                <img width={60} height={60} src={e.image ? e.image : defaultLocation.src} alt={e.name} />
                            </div>
                            <div className='location_main margin-l-10'>
                                <div className='city_name semi-bold padding-b-5 font-14'>{e.name}</div>
                                {
                                    e.location &&
                                    <div className='location_name font-11 more-grey'>{e.location}</div>
                                }
                            </div>
                        </div>                 
                    )
                })
            }

            {
            locationError && 
                <div className="error">Someting went wrong :(</div>
            }
        </div>
    </div>
  )
}
