import Link from 'next/link';
import styles from "styles/listing.module.css"


export default function ListingCard(props) {
    const listingData = props.listingData;
    return (
        <div className='padding-15'>
            <div className={`${styles.listin_card}`}>
                {
                    listingData.map((data, index) => {
                        return (
                            <div key={index} className='relative'>
                                <div className={`${styles.touch_box}`}>
                                    <Link className='pointer' href={`${data.code}`}>
                                        <a href={`${data.code}`}>
                                            <div className="relative">
                                                {
                                                    data.image ? 
                                                    <div className={`flex ${data.image.length < 2 ? `${styles.full_img}` : ''} ${styles.image_main}`}>
                                                        {
                                                            data.image.map((data, index) => {
                                                                return (
                                                                    <div className={`flex ${styles.image_internal}`} key={index}>
                                                                        <img className='full-width' width={100} height={150} src={data} alt="Restaurant Image" loading="lazy"/>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                    : <div className={`flex ${styles.image_internal}`}></div>
                                                }

                                                {
                                                    data.restaurant_tags &&
                                                    <div className={`absolute flex ${styles.res_feature}`}>
                                                        {
                                                            data.restaurant_tags.map((data, i) => {
                                                                return(
                                                                    <div key={i} className={`${styles.feature_list}`}
                                                                        style={{
                                                                            backgroundColor: `rgba(${data.bg_color})`,
                                                                            color: `rgba(${data.text_color})`,
                                                                        }}>
                                                                        {data.title}
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                }
                                            </div>

                                            <div className={`${styles.center_content}`}>
                                                <div className='flex align-v-center flex-between'>
                                                    {
                                                        data.name && <div className={`${styles.res_name}`}>
                                                            {data.name}
                                                        </div>
                                                    }
                                                    {
                                                        data.critic_review &&
                                                        <div className={`${styles.rating_num}`}>
                                                            <svg width="46" height="24" viewBox="0 0 46 24" xmlns="http://www.w3.org/2000/svg">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <g>
                                                                        <rect fill="#43A047" width="46" height="24" rx="12"/>
                                                                        <path d="M37.986 10.641a.293.293 0 0 0-.237-.2l-3.1-.45-1.386-2.809a.293.293 0 0 0-.526 0l-1.386 2.81-3.1.45a.293.293 0 0 0-.163.5l2.244 2.186-.53 3.087a.293.293 0 0 0 .425.31L33 15.065l2.773 1.458a.293.293 0 0 0 .425-.309l-.53-3.087 2.244-2.187a.293.293 0 0 0 .074-.3z" fill="#FFF" fillRule="nonzero"/>
                                                                    </g>
                                                                    <text fontSize="12" fontWeight="400" fill="#FFF">
                                                                        <tspan x="8" y="16">{data.critic_review.rating.toFixed(1)}</tspan>
                                                                    </text>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    }
                                                </div>
                                                <div className={`font-12 flex ${styles.res_address}`}>
                                                   <div>2.4 Kms | {data.location} </div> {
                                                        data.more_deals && <span className={`${styles.list_outlet}`}> &nbsp; | {data.more_deals}</span>
                                                    }
                                                </div>
                                                <div className={`font-12 flex ${styles.cuisine_aprox}`}>
                                                    <div className={`flex align-v-center ${styles.multi_cuisines}`}>
                                                        <div className={`${styles.cuisine_aprox_icon}`}>
                                                            <svg width="18" height="14" viewBox="0 0 18 14" xmlns="http://www.w3.org/2000/svg">
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path d="M15.745 11.709a6.823 6.823 0 1 0-13.647 0h13.647z" fill="#9E9E9E" />
                                                                    <path d="M15.745 11.709a6.823 6.823 0 1 0-13.647 0h13.647z" stroke="#212121" strokeWidth=".5" strokeLinecap="round" strokeLinejoin="round" />
                                                                    <path d="M9.691 4.917H8.244a.356.356 0 0 1-.355-.355v-.168c0-.195.16-.355.355-.355H9.69c.195 0 .354.16.354.355v.168a.355.355 0 0 1-.354.355" fill="#9E9E9E" />
                                                                    <path d="M9.691 4.917H8.244a.356.356 0 0 1-.355-.355v-.168c0-.195.16-.355.355-.355H9.69c.195 0 .354.16.354.355v.168a.355.355 0 0 1-.354.355z" stroke="#212121" strokeWidth=".5" strokeLinecap="round" strokeLinejoin="round" />
                                                                    <path fill="#E5E5E5" d="M1.138 11.706h15.659l-.743 1.67H2.246z" />
                                                                    <path stroke="#212121" strokeWidth=".5" strokeLinecap="round" strokeLinejoin="round" d="M1.138 11.706h15.659l-.743 1.67H2.246zM5.9 1.372s-.859.81-.15 1.707c.711.897-.065 1.44-.065 1.44M12.314 1.692s-.859.81-.149 1.707-.066 1.44-.066 1.44M9.238 1.138s-1.085.873-.028 1.768" />
                                                                    <path d="M3.16 9.372s.92-1.916 2.143-2.69" stroke="#FFF" strokeWidth=".5" strokeLinecap="round" strokeLinejoin="round" />
                                                                    <path d="M5.696 6.406c0 .024-.02.045-.045.045a.045.045 0 0 1-.045-.045c0-.025.02-.046.045-.046.025 0 .045.02.045.046z" stroke="#FFF" strokeWidth=".415" strokeLinecap="round" strokeLinejoin="round" />
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        {data.cuisines}
                                                    </div>
                                                    <div className='flex align-v-center'>
                                                        <div className={`${styles.cuisine_aprox_icon}`}>
                                                            <svg width="15" height="16" viewBox="0 0 15 16" xmlns="http://www.w3.org/2000/svg">
                                                                <defs>
                                                                    <path d="M.535.407C.37.407.237.52.237.66c0 .14.132.253.298.253H1.94c.42.003.786.21.968.508H.535c-.166 0-.298.114-.298.253 0 .14.132.255.298.255h2.488c-.05.472-.52.845-1.093.845H.535c-.166 0-.298.113-.298.253 0 .141.132.254.298.254H1.94c.601.005 1.087.42 1.087.93v1.521c0 .14.134.253.3.253.164 0 .3-.113.3-.253V4.211c0-.49-.293-.924-.737-1.184.418-.244.7-.643.732-1.098h.606c.165 0 .299-.115.299-.255s-.134-.253-.299-.253h-.677a1.42 1.42 0 0 0-.33-.508h1.007c.165 0 .299-.113.299-.253S4.393.407 4.228.407H.535z" id="c0junqu7ta" />
                                                                </defs>
                                                                <g fill="none" fillRule="evenodd">
                                                                    <path d="M6.745 2.059a6.382 6.382 0 1 1 0 12.764 6.382 6.382 0 0 1 0-12.764" fill="#9E9E9E" />
                                                                    <path d="M6.745 2.059a6.382 6.382 0 1 1 0 12.764 6.382 6.382 0 0 1 0-12.764z" stroke="#212121" strokeWidth=".5" strokeLinecap="round" strokeLinejoin="round" />
                                                                    <path d="M8.592 6.665h-.678a1.4 1.4 0 0 0-.329-.508h1.007c.164 0 .298-.114.298-.253 0-.141-.134-.253-.298-.253H4.898c-.164 0-.298.112-.298.253 0 .14.134.253.298.253h1.407c.42.003.785.21.966.508H4.898c-.164 0-.298.113-.298.253s.134.254.298.254h2.488c-.05.473-.521.845-1.091.845H4.898c-.164 0-.298.114-.298.253 0 .141.134.255.298.255h1.407c.6.004 1.087.419 1.087.93v1.52c0 .142.133.255.298.255.164 0 .3-.113.3-.254V9.454c0-.49-.292-.923-.737-1.184.418-.243.701-.643.732-1.098h.607c.164 0 .298-.113.298-.254 0-.14-.134-.253-.298-.253" fill="#FFF" />
                                                                    <g transform="translate(4.364 5.244)">
                                                                        <mask id="qjp7838l6b" fill="#fff">
                                                                            <use xlinkHref="#c0junqu7ta" />
                                                                        </mask>
                                                                        <path fill="#FFF" mask="url(#qjp7838l6b)" d="M-3.96 10.182H8.724V-3.79H-3.96z" />
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        {data.cost_for_two}
                                                    </div>
                                                </div>
                                            </div>

                                            <div className={`flex align-v-center flex-between ${styles.res_offer}`}>
                                                <div className='flex align-v-center'>
                                                    <svg width="18" height="18" viewBox="0 0 15 15" xmlns="http://www.w3.org/2000/svg">
                                                        <g fill="none" fillRule="evenodd">
                                                            <path fill="#D95931" fillRule="nonzero" d="m7.5 14.287-2.318.346-1.671-1.642-2.079-1.083-.387-2.31L0 7.5l1.045-2.097.387-2.311 2.079-1.083L5.182.367 7.5.713 9.818.367l1.671 1.642 2.079 1.083.387 2.31L15 7.5l-1.045 2.097-.387 2.311-2.079 1.083-1.671 1.642z" />
                                                            <text fontFamily="" fontSize="10" fontWeight="bold" fill="#FFF">
                                                                <tspan x="3" y="11">%</tspan>
                                                            </text>
                                                        </g>
                                                    </svg>
                                                    <div className={`${styles.offer_text}`}>
                                                        25% Off
                                                    </div>
                                                </div>
                                                {
                                                    data.more_deals &&
                                                    <div className={`${styles.more_deals}`}>{data.more_deals}</div>
                                                }
                                            </div>
                                        </a>
                                    </Link>
                                    <div className={`flex align-v-center flex-between ${styles.btm_cta}`}>
                                        <div className={`pointer ${styles.menu_btn}`}>
                                            <Link href={data.code}>
                                                View menu
                                            </Link>
                                        </div>

                                        <Link href={data.code}>
                                            <div className={`pointer ${styles.book_a_table}`}>
                                                Book a table
                                            </div>
                                        </Link>
                                    </div>
                                    <div className={`absolute pointer ${styles.fav_res}`}>
                                        <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <g fill="none" fillRule="evenodd">
                                                <circle fillOpacity=".9" fill="#FFF" opacity=".8" cx="12" cy="12" r="11.143" />
                                                <path d="M3.429 3.429h18v18h-18z" />
                                                <path d="M18.857 9.81c0-2.352-1.59-2.846-2.93-2.846-1.247 0-2.653 1.348-3.088 1.867a.553.553 0 0 1-.82 0c-.436-.52-1.842-1.867-3.09-1.867C7.59 6.964 6 7.458 6 9.81c0 1.532 1.549 2.955 1.565 2.972l4.864 4.687 4.854-4.679c.026-.025 1.574-1.448 1.574-2.98zm1.072 0c0 2.01-1.842 3.692-1.917 3.767l-5.215 5.022a.52.52 0 0 1-.368.151.52.52 0 0 1-.369-.15l-5.223-5.04c-.067-.058-1.908-1.74-1.908-3.75 0-2.452 1.498-3.917 4-3.917 1.466 0 2.838 1.155 3.5 1.808.66-.653 2.034-1.808 3.498-1.808 2.503 0 4.002 1.465 4.002 3.917z" fill="#616161" />
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}