import HeaderBackNav from '@/components/headers/HeaderBackNav'
export default function MyPayEazy(props) {
    const {userData} = props
    return (
        <>
            <HeaderBackNav title="My PayEazy" hideBack={true}/>
            <div className='back_header_content padding-15'>
                <h2 className=''> Hi, {userData.name}! <br /><br /> Your PayEazy is comming soon...</h2>
            </div>
        </>
    )
}