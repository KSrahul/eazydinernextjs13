export const FoodTrends = (props) => {
    const {title} = props.data
    const seoData = {
        title: title,
        description : "Read from 1000+ Food Trends curated by India's best Food Critics at eazydiner.com. Discover expert inputs on latest food trends, different cuisines, deals offered by restaurants, in top cities in India"
    }

    return seoData
}


