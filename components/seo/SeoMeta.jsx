import Head from 'next/head'

export const SeoMeta = (props) => {
  const seoMetaData = { ...defaultMetaData, ...props.metaData }
  return (
    <>
        <Head>
            <link rel="icon" href="/favicon.png" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
            <title>{seoMetaData.title}</title>
            <meta name="description" content={seoMetaData.description} />
        </Head>
    </>
  )
}


const defaultMetaData = {
  title: "EazyDiner",
  description: "EazyDiner provide you with top-notch deals. Get minimum guaranteed 25% up to 50% OFF at EazyDiner Prime-enabled restaurants",
}
