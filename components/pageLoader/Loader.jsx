export default function LoadingBar(props) {
    const lineCss = {
        position: "fixed",
        top: "0",
        height: "4px",
        backgroundImage: "linear-gradient(to right,#ff8000,#fa4616)",
        zIndex: "3",
        transition: "0.3s"
    }
  return (
    <>    
        <div style={{...lineCss, width: `${props.progress == 100 ? 0 : props.progress}%`, opacity: props.progress == 100 ? 0 : 1}} className="line_loader"></div>
    </>
  )
}
