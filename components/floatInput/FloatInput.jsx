import { useState } from 'react';
export default function FloatInput(props) {
    const {label, value, handleChange, autoFocus,  ...restAtr} = props;
    const [focused, setFocused] = useState(false);
    const handleFocus = () => {
        setFocused(true)
    }
  return (
    <>
        <div className={`input_container full-width relative ${value && "filled_input"}`}>
            <input className="full-width float_input" {...restAtr} value={value} autoFocus={autoFocus} onChange={handleChange} onBlur={handleFocus} focused={focused.toString()} />
            <label className={`absolute float_label`}>{label}</label>
        </div>
    </>
  )
}
