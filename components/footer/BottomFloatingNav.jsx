import Link from 'next/link'
import home from './assets/home.svg'
import selectedHome from './assets/home_selected.svg'
import booking from './assets/bookings.svg'
import selectedBooking from './assets/bookings_selected.svg'
import prime from './assets/prime.svg'
import selectedPrime from './assets/prime_selected.svg'
import profile from './assets/profile.svg'
import selectedProfile from './assets/profile_selected.svg'
import {keyConstant} from '../../constant'
import GetImgageSrc from "@/helpers/ImageBuilder"
import noPhoto from "@/public/icons/login/profile_135.svg"

export default function BottomFloatingNav(props) {
    const {pageName, userDetails} = props;
    return (
        <>
            <div className={`flex full-width flex-between text-center fixed bottom_nav bottom-0 bg-white bottom_main`}>
                {
                    navMenu.map((e, i) => {
                        // console.log(userDetails, e.userPath)
                        return(
                            <div className={`flex-1 nav_link self-center ${e.key == keyConstant.PAYEAZY ? `relative payeazy_link` : ''} ${pageName == e.key ? `active_page` : ''}`} key={i}>
                                <Link href={`${userDetails && e.userPath ? e.userPath : e.path}`}>
                                    <div className="pointer">
                                        {
                                            userDetails && keyConstant.PROFILE == e.key ?
                                                <img className='radius-50' 
                                                    src={userDetails.data.image ? userDetails.data.image : noPhoto.src} 
                                                    width={e.width} 
                                                    height={e.height} 
                                                    alt={e.key}/>
                                            : <img className='bottom_img' 
                                                src={ `${pageName == e.key ? e.selectedIcon : e.icon}`} 
                                                width={e.width} 
                                                height={e.height} 
                                                alt={e.key} />
                                        }
                                        {
                                            e.key != "payeazy" &&
                                            <div className={`font-10 other-light-grey margin-t-4 uppercase nave_name`}>
                                                {e.name}
                                            </div>
                                        }
                                    </div>
                                </Link>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
}

const navMenu = [
    {
        name: "HOME",
        key: keyConstant.HOME,
        icon: `${home.src}`,
        selectedIcon: `${selectedHome.src}`,
        width: `${home.width}`,
        height: `${home.height}`,
        path: "/"
    },
    {
        name: "BOOKINGS",
        key: keyConstant.BOOKING,
        icon: `${booking.src}`,
        selectedIcon: `${selectedBooking.src}`,
        width: `${booking.width}`,
        height: `${booking.height}`,
        path: "/bookings",
        // userPath: "/user/my-bookings"
    },
    {
        name: "PAYEAZY",
        key: keyConstant.PAYEAZY,
        icon: `${GetImgageSrc('home/icons/payeazy_app_launch_dec_2020.png')}`,
        selectedIcon: `${GetImgageSrc('home/icons/payeazy_app_launch_dec_2020.png')}`,
        path: "/payeazy",
        height: "70px",
        width: "70px",
    },
    {
        name: "PRIME",
        key: keyConstant.PRIME,
        icon: `${prime.src}`,
        selectedIcon: `${selectedPrime.src}`,
        width: `${prime.width}`,
        height: `${prime.height}`,
        path: "/prime",
        // userPath: "/user/my-prime"
    },
    {
        name: "account",
        key: keyConstant.PROFILE,
        icon: `${profile.src}`,
        selectedIcon: `${selectedProfile.src}`,
        width: `${profile.width}`,
        height: `${profile.height}`,
        path: "/account",
        // userPath: "/user/my-account"
    },
]
