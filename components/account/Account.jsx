import loginIcon from "public/icons/login/profile_135.svg"
import leftAero from "public/icons/login/left_arrow.svg"
import payeazy from "public/icons/login/pay-eazy.svg"
import eazyPoints from "public/icons/login/eazy-points.svg"
import referral from "public/icons/login/refer.svg"
import partnerOffer from "public/icons/login/partner-offers.svg"
import foodTrends from "public/icons/login/food-trends.svg"
import about from "public/icons/login/about.svg"
import privacyPolicy from "public/icons/login/privacy-policy.svg"
import tearmsAndCondition from "public/icons/login/tearms-and-condition.svg"
import feedback from "public/icons/login/feedback.svg"
import ContextAllUpdate from "@/helpers/ContextAllUpdate"
import { MODAL_LOGIN, SHOW_MODAL } from '@/context/ActionType';
import { AccordionMain } from '@/components/accordion/AccordionMain'
import styles from "styles/profile.module.css"

export default function Account() {
    const {updateStates} = ContextAllUpdate();
    const showLogin = () => {
        updateStates({
            type: SHOW_MODAL,
            modalToShow: MODAL_LOGIN
        })
    }
  return (
    <div className={`margin-b-80`} style={{"marginTop": "78px"}}>
        <div onClick={showLogin}>
        <div className={`flex white fixed full-width top-0 flex-between align-v-center ${styles.header_color}`}>
            <div className={`flex`}>
            <div>
                <img width={32} height={32} src={loginIcon.src} alt="Non login user"/>
            </div>
            <div className={`padding-l-15`}>
                <div className={`bold font-15 padding-b-3`}>Login / Signup</div>
                <div className={`italic font-13`}>Signup now to earn 500 eazypoints</div>
            </div>
            </div>
            <div className='flex'>
            <img width={9} height={15} src={leftAero.src} alt="leftAero"/>
            </div>
        </div>
        <div className="" onClick={showLogin}>
            <img className={`full-width`} src="https://d4t7t8y8xqo0t.cloudfront.net/eazymedia/banner%2FSingup-Banner-M-site.png" alt="signup" />
        </div>
        </div>
        <div className={`padding-15`}>
        <div className={`padding-b-12 padding-t-10 accor_main`}>
            <div className={`${styles.accordion_content}`}>
                <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                <div className='margin-r-10 flex'>
                    <img src={partnerOffer.src} alt=""  />
                </div>
                <div>Partner Offers</div>
                </div>

                <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                <div className='margin-r-10 flex'>
                    <img src={foodTrends.src} alt=""  />
                </div>
                <div>Food Trends</div>
                </div>

            </div>
        </div>

        <div className={`padding-b-12 padding-t-10 accor_main`}>
            <AccordionMain id={0} 
            title="EazyPrograms"
            activePanel={true}
            content={
                <>
                <div className={`margin-t-20`}>
                    <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                    <div className='margin-r-10 flex'>
                        <img src={payeazy.src} alt="account Image" width={24} height={24}  />
                    </div>
                    <div>PayEazy</div>
                    </div>

                    <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                    <div className='margin-r-10 flex'>
                        <img src={eazyPoints.src} alt="account Image" width={24} height={24}  />
                    </div>
                    <div>EazyPoints</div>
                    </div>

                    <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                    <div className='margin-r-10 flex'>
                        <img src={referral.src} alt="account Image" width={24} height={24}  />
                    </div>
                    <div>Referral</div>
                    </div>
                </div>
                </>
            }>
            </AccordionMain>
        </div>

        <div className={`padding-b-12 padding-t-10 accor_main`}>
            <AccordionMain id={1}
                title="Help"
                activePanel={true}
                content={
                    <>
                    <div className={`margin-t-20`}>
                        <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                        <div className='margin-r-10 flex'>
                            <img src={about.src} alt=""  />
                        </div>
                        <div>About</div>
                        </div>

                        <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                        <div className='margin-r-10 flex'>
                            <img src={privacyPolicy.src} alt=""  />
                        </div>
                        <div>Privacy Policy</div>
                        </div>

                        <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                        <div className='margin-r-10 flex'>
                            <img src={tearmsAndCondition.src} alt=""  />
                        </div>
                        <div>Terms & Condition</div>
                        </div>
                        
                        <div className={`flex font-14 align-v-center margin-b-15 grey-dark pointer ${styles.list_menus}`}>
                        <div className='margin-r-10 flex'>
                            <img src={feedback.src} alt=""  />
                        </div>
                        <div>Feedback</div>
                        </div>
                    </div>
                    </>
                }>
            </AccordionMain>
        </div>
        </div>
    </div>
  )
}
