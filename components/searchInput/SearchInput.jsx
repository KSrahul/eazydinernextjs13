import {useEffect, useRef} from 'react'
import { SEARCH_RESULT, REMOVE_SEARCH_RESULT } from '@/context/ActionType';
import ContextAllUpdate from "@/helpers/ContextAllUpdate"

export default function SearchInput(props) {
    const {states, updateStates} = ContextAllUpdate();
    const textInput = useRef(null);

    const updateSearch = (type, data) =>{
        updateStates({
            type: type,
            data: data
        })
    }
    const searchText = (e) =>{
        updateSearch(SEARCH_RESULT, e.target.value)
    }
    const removeSearchValue = () =>{
        updateSearch(REMOVE_SEARCH_RESULT, "");
        textInput.current.focus();
    }

    useEffect(() => {
        updateSearch(REMOVE_SEARCH_RESULT, "")
    }, [])
    
  return (
    <div className='relative main_input'>
        <div className='absolute search_icon flex'>
            <svg width="18" height="18" viewBox="0 0 22 22">
                <g fill="none" fillRule="evenodd">
                    <path fill="black" fillRule="nonzero" d="M18.465 17.319l-3.728-3.81a6.13 6.13 0 001.484-3.995c0-3.426-2.838-6.214-6.325-6.214-3.488 0-6.325 2.788-6.325 6.214 0 3.426 2.837 6.214 6.325 6.214 1.309 0 2.557-.388 3.623-1.124l3.757 3.838c.157.16.368.249.595.249.214 0 .417-.08.571-.226a.801.801 0 00.023-1.146zM9.895 4.92c2.578 0 4.676 2.06 4.676 4.593s-2.098 4.593-4.675 4.593c-2.578 0-4.675-2.06-4.675-4.593s2.097-4.593 4.675-4.593z"></path>
                    <path d="M0 0H22V22H0z"></path>
                </g>
            </svg>
        </div>
        <input className='search_input full-width outline-0' autoFocus={true} onInput={searchText} value={states.searchResult} type="text" placeholder={props.placeholder} />
        <div className='absolute input_action'>
            {
                states.searchResult && 
                <div className='remove_value' onClick={removeSearchValue}>
                    <svg width="20" height="20" viewBox="0 0 22 22">
                        <g fill="none" fillRule="evenodd">
                            <path fill="#BDBDBD" d="M11 21C5.477 21 1 16.523 1 11S5.477 1 11 1s10 4.477 10 10-4.477 10-10 10zm.95-10l2.853-2.852a.672.672 0 10-.95-.951L11 10.049 8.148 7.197a.672.672 0 10-.951.95L10.049 11l-2.852 2.852a.672.672 0 00.95.951L11 11.951l2.852 2.852a.672.672 0 10.951-.95L11.951 11z"></path>
                            <path d="M0 0H22V22H0z"></path>
                        </g>
                    </svg>
                </div>
            }
            {
                !states.searchResult && 
                <div className='mike pointer flex'>
                    <svg width="22" height="22" fill='red'>
                        <path d="M 12 2 C 10.343 2 9 3.343 9 5 L 9 11 C 9 12.657 10.343 14 12 14 C 13.657 14 15 12.657 15 11 L 15 5 C 15 3.343 13.657 2 12 2 z M 5 11 C 5 14.525296 7.6093644 17.433226 11 17.919922 L 11 21 L 13 21 L 13 17.919922 C 16.390636 17.433226 19 14.525296 19 11 L 17 11 C 17 13.761 14.761 16 12 16 C 9.239 16 7 13.761 7 11 L 5 11 z"></path>    
                    </svg>
                </div>
            }

            {/* <div className="speak_mike">
                <svg width="20" height="20" viewBox="0 0 100 100">
                    <g stroke="red" strokeWidth="15">
                        <path d="M20,20 20,80">
                            <animate attributeName="d" values="M20,40 20,60;M20,20 20,80;M20,40 20,60" dur="1s" repeatCount="indefinite"></animate>
                        </path>
                        <path d="M50,10 50,90">
                            <animate attributeName="d" values="M50,10 50,90;M50,40 50,60;M50,10 50,90" dur="1s" repeatCount="indefinite"></animate>
                        </path>
                        <path d="M80,20 80,80">
                            <animate attributeName="d" values="M80,40 80,60;M80,20 80,80;M80,40 80,60" dur="1s" repeatCount="indefinite"></animate>
                        </path>
                    </g>
                </svg>
            </div> */}
        </div>
    </div>
  )
}

