export default function loading() {
  return (
    <div className="back_header_content">
        <div className="padding-15">
            Loading...
        </div>
    </div>
  )
}
