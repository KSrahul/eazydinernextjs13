import HeaderBackNav from "@/components/headers/HeaderBackNav";
import { FOOD_TRENDS_DETAILS } from "api";
import styles from "styles/trend.module.css"



async function getData(context){
    const apiCall = await fetch(`${FOOD_TRENDS_DETAILS}/${context.params.detail}`, {cache: "no-store"})
    return apiCall.json()

  }

export default async function detail(context) {
  const data = await getData(context)

  return (
    <div>
        {
            data ?
            <>
                <HeaderBackNav title={`Food Trends ${data.data.title}`}  showShare={true}  />   
                <div className='back_header_content padding-15'>
                    <div className={`radius-8 ${styles.trend_wrapper}`}>
                        <div className="margin-b-10">
                            <div className={`${styles.html_class}`} dangerouslySetInnerHTML={{__html: data.data.description}}></div>
                        </div>
                    </div>
                </div>
            </>
            : 
            <>
                <HeaderBackNav title={`Food Trends`} />
                <h2 className='back_header_content padding-t-50 padding-15 text-center red'>
                    No Data Found :(
                </h2>
            </>    
        }
    </div>
  )
}
