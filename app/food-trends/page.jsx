import { FOOD_TRENDS } from "api"
import Link from 'next/link'
import url from "url"
import styles from "styles/trend.module.css"
import HeaderBackNav from "@/components/headers/HeaderBackNav"
import Image from "next/image"


async function getData(query){
  console.log(`${FOOD_TRENDS}`, query)
  const fullPathNamess = url.format({ pathname: 'food-trends/', query: query });
  const apiCall = await fetch(`${FOOD_TRENDS.replace("food-trends", "")}${fullPathNamess}`, {cache: "no-store"})
  return apiCall.json()
}

export default async function Page(context) { 
  const data = await getData(context.searchParams)
  return (
    <div className="back_header_content">
      <div className={`bottom_shadow bg-white ${styles.trends_header}`} data-data={JSON.stringify(data)}>
          <HeaderBackNav title="Food Trends" hideShadow={true} />
          <div className="flex flex-between">
              {
                  data.filters.map((e, i) => {
                    const queryWithFilter = context.searchParams;
                    queryWithFilter.filter = e;
                      return(
                          <div key={i} className={`${data.filter_applied.includes(e) ? styles.active_tab : ''}`}>
                              <Link className={`pointer padding-15 uppercase block grey-light`} href={{
                                  pathname: '/food-trends/',
                                  query: {...queryWithFilter},
                                }}>{e}</Link>
                          </div>
                      )
                  })
              }
          </div>
      </div>
      <div className="padding-15">
        {data.data.map((e, i) => (
            <Link key={i} href={e.url.replace("https://www.eazydiner.com", "")}>
                <div key={i} className={`pointer block radius-8 margin-b-15 ${styles.trend_card}`}>
                    <div className={`${styles.list_img}`}>
                    <Image
                        src={e.image}
                        blurDataURL={e.image}
                        width={400}
                        height={200}
                        alt={e.title}
                        placeholder="blur"
                        className={`full-width ${styles.trend_img}`}
                        loading={"lazy"}
                    />
                    </div>
                    <div className="padding-10">
                    <div className="grey-dark font-18 margin-b-10">{`${e.title.length >= 75 ? `${e.title.substring(0, 75)}...` : e.title}`}</div>
                    <div className="margin-b-10 grey-light font-12">{e.subtitle}</div>
                    <div className="font-12 grey-light">{e.author} {e.publish_date}</div>
                    </div>
                </div>
            </Link>
        ))}
      </div>
      {/* <div className="flex flex-between">
        {data.filters.map((e, i) => {
          queryWithFilter.filter = e;
          const href = {
            pathname: '/food-trends/',
            query: queryWithFilter,
          };
          const fullPathName = url.format({ pathname: '/food-trends/', query: queryWithFilter });

          return (
            <div key={i} style={{"marginBottom": "20px"}}>
              <Link href={fullPathName}>
                <div className={`pointer padding-15 uppercase grey-light`}>{e}</div>
              </Link>
            </div>
          );
        })}
      </div> */}
    </div>
  )
}
