export const ServerSideCache = (res) => {
    return(
        res.setHeader(
            'Cache-Control',
            'public, s-maxage=0, stale-while-revalidate=864000'
        )
    )
}
