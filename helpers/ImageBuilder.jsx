export default function GetImgageSrc(path, width, height) {
    const imagDomain = process.env.NEXT_PUBLIC_IMAGE_URL;
    let imgagePath = ''
    if(width || height){
        imgagePath = `resized/${width}X${height}/${path}`;
    }else{
        imgagePath = `eazymedia/${path}`;
    }
    return `${imagDomain}${imgagePath}`;
}