export default function TokenCusId(req, res) {
    const tokenCusId = {
        token: "",
        cusTomerId: ""
    }

    if(req.cookies.token){
        tokenCusId.token = req.cookies.token
    }else{
        tokenCusId.token = ""
    }

    if(req.cookies.cusId){
        tokenCusId.cusTomerId = req.cookies.cusId
    }else{
        tokenCusId.cusTomerId = ""
    }

    return tokenCusId;
}
