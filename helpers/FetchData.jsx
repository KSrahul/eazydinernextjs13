import axios from "axios";
import { useState, useEffect } from "react";
import { ServerSideCache } from "@/helpers/ServerSideCache";


export const ClientSide = (props, enabled = false) => {
  const {url, method, data, headers, params} = props;
  const [apiData, setApiData] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState(false);


  const resetData = () => {
    setApiData(false);
    setLoading(true);
    setError(false);
  }
  
  const apiCall = () => {
    setLoading(true);
    axios({
      url: url,
      method: method,
      data: data,
      params: params,
      headers: {'Content-Type': 'multipart/form-data', ...headers}
    }).then((response) =>{
        setApiData(response);
        setLoading(false);
    }).catch((error) => {
        setError(error);
        setLoading(false);
    })
  }

  const onAction = () => {
    apiCall();
  }

  useEffect(() => {
    if(enabled){
      apiCall()
    }
  }, [url, enabled]);

  return [ isLoading, apiData, error, onAction, resetData];
}


export const ServerSide = async (url, req, res) => {
  const token = userToken(req.cookies?.token)
  let data = "";
  const apiCall =  axios({
    url: url,
    method: 'get',
    headers: {
      ...
      token,
    }
  }).then((response) =>{
    data = response.data
    // ServerSideCache(res)
  }).catch((error) => {
    data = ""
  })

  await apiCall;

  return data
}

export const userToken = (userToken) => {
  const header = {
    "Authorization": `Bearer:${userToken}`,
    'Content-Type': 'multipart/form-data'
  }
  return header
}