import { deleteCookie } from 'cookies-next';
import {userDataApi} from 'api'
import axios from 'axios'

export const FetchUserData = async (req, res) => {
  const deleteUserTokenId = () => {
    deleteCookie('cusId', { req, res });
    deleteCookie('token', { req, res });
  }
    let userData = "";
    if(req.cookies.token && req.cookies.cusId){
      const getTokenIdData = await UserApiCall(req.cookies.token, req.cookies.cusId)
      if(getTokenIdData.data){
        userData = getTokenIdData
      }else{
        deleteUserTokenId();
      }
    }else{
      userData = "";
      deleteUserTokenId();
    }

    return userData
}

export const UserApiCall = async (token, customerId) => {
  let response = "";
    await axios({
      method: 'get',
      url: `${userDataApi(customerId)}`,
      headers: {
        "Authorization": `Bearer:${token}`,
        'Content-Type': 'multipart/form-data'
      }
    }).then((data) => {
      response = data.data
    }).catch(() => {
      response = "";
    })

    return response
}
