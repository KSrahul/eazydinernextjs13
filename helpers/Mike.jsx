export const Mike = () =>{
    const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    if (SpeechRecognition) {
        console.log("Yes")
        var initSpeach = new SpeechRecognition();
        initSpeach.continuous = true;
        initSpeach.onstart = function() {
            console.log("Voice Activate")
        }

        initSpeach.onend = function() {
            console.log("Voice End")
        }

        initSpeach.onerror = function() {
            console.log("Voice Error")
        }

        initSpeach.onresult = function(voiceResult) {
            console.log(voiceResult)
            const getResult = voiceResult.results[0][0].transcript;
            console.log(getResult)
        }
        initSpeach.start();

    } else {
        console.log("No")
    }
}
