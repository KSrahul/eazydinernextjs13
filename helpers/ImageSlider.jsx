import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

export default function ImageSlider(props) {
  // console.log(props.showIndicators)
  return (
    <>
      <Carousel
          showStatus={false} 
          showArrows={false} 
          showIndicators={props.showIndicators} 
          swipeable={true} 
          showThumbs={false}>

          {props.children}
          
      </Carousel>
    </>
  )
}
