export const FetchUserLocation = (req, res) => {
    let userLocation = "";
    if(req.cookies.userLocation){
        userLocation =  JSON.parse(req.cookies.userLocation)
    }else{
        userLocation = defaultLocation();
    }
    return userLocation
}

export const defaultLocation = () => {
    const locationDefault = {
        type: "location",
        loc_type: "area",
        name: "DLF Phase 3",
        code: "dlf-phase-3-gurgaon",
        location: "Gurgaon",
        city_code: "delhi-ncr",
        city_name: "Delhi NCR",
        lat :"28.491",
        long :"77.1066",
        image :"https://d4t7t8y8xqo0t.cloudfront.net/testing/eazymedia/search/delhi.png",
        default: true
    }

    return locationDefault
}