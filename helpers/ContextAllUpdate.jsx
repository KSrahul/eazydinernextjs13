import { useContext } from 'react';
import { GlobalContextData } from '@/context/GlobalContext';

export default function ContextAllUpdate() {
    const {states, updateStates} =  useContext(GlobalContextData);
    return {states, updateStates}
}
