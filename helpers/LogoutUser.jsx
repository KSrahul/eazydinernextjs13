import { deleteCookie } from 'cookies-next';

export default function LogoutUserFn() {
    const logout = confirm("Do you realy wat to logout?");

    if(logout){
        deleteCookie("token");
        deleteCookie("cusId");
        location.reload();
    }
    return {logout}
}
